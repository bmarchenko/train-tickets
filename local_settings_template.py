DEBUG = True
TEMPLATE_DEBUG = DEBUG
BASE_REQUEST_DELAY = 60
ADMINS = (
    ('Bogdan Marchenko', 'bogdan.marko@gmail.com'),
)
CELERY_SEND_TASK_ERROR_EMAILS = True

MANAGERS = ADMINS
CELERY_SEND_TASK_ERROR_EMAILS = ("",)
ALLOWED_HOSTS = ('train-tickets.com.ua', 'www.train-tickets.com.ua', 'sms-kvytok.tk', 'www.sms-kvytok.tk')
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'tickets_db',                      # Or path to database file if using sqlite3.
        'USER': '',                      # Not used with sqlite3.
        'PASSWORD': '',                  # Not used with sqlite3.
        'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
    }
}

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'youremail@gmail.com'
EMAIL_HOST_PASSWORD = 'yourpassword'
EMAIL_PORT = 587

SMS_LOGIN = 380637
SMS_KEY = ""