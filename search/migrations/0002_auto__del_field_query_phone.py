# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Query.phone'
        db.delete_column(u'search_query', 'phone')


    def backwards(self, orm):
        # Adding field 'Query.phone'
        db.add_column(u'search_query', 'phone',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)


    models = {
        u'search.cities': {
            'Meta': {'object_name': 'Cities'},
            'country': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'search.query': {
            'Meta': {'object_name': 'Query'},
            'datetime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kype': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'platzcart': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'query': ('django.db.models.fields.CharField', [], {'max_length': '1000'})
        },
        u'search.station': {
            'Meta': {'object_name': 'Station'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'name_ukr': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        }
    }

    complete_apps = ['search']