# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Station'
        db.create_table(u'search_station', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_ukr', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('name_ru', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
        ))
        db.send_create_signal(u'search', ['Station'])

        # Adding model 'Cities'
        db.create_table(u'search_cities', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('country', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal(u'search', ['Cities'])

        # Adding model 'Query'
        db.create_table(u'search_query', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('kype', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('platzcart', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('phone', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('query', self.gf('django.db.models.fields.CharField')(max_length=1000)),
            ('datetime', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'search', ['Query'])


    def backwards(self, orm):
        # Deleting model 'Station'
        db.delete_table(u'search_station')

        # Deleting model 'Cities'
        db.delete_table(u'search_cities')

        # Deleting model 'Query'
        db.delete_table(u'search_query')


    models = {
        u'search.cities': {
            'Meta': {'object_name': 'Cities'},
            'country': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        },
        u'search.query': {
            'Meta': {'object_name': 'Query'},
            'datetime': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kype': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'phone': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'platzcart': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'query': ('django.db.models.fields.CharField', [], {'max_length': '1000'})
        },
        u'search.station': {
            'Meta': {'object_name': 'Station'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'name_ukr': ('django.db.models.fields.CharField', [], {'max_length': '300'})
        }
    }

    complete_apps = ['search']