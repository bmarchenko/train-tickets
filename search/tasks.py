# -*- coding: utf-8 -*-
from celery import task
from search.forms import QueryForm
from django.core.mail import send_mail
from search.models import Station
import urllib2, urllib
import math
from django.conf import settings
try:
    import json
except ImportError:
    import simplejson as json

def send_message(data, message, short_msg):
    if data['phone']:
        if len(short_msg) <= 45:
            short_msg += short_msg+u' train-tickets.com.ua'
        send_mail(u"{0};{1};".format(settings.SMS_LOGIN, settings.SMS_KEY), short_msg, 'info@sms-kvytok.tk',
            [data['phone']+"@mail.smsukraine.com.ua",], fail_silently=False)
    if data['email']:
        send_mail(u"Повідомлення від Train Tickets", message, 'info@sms-kvytok.tk',
                  [data['email'],], fail_silently=False)


@task(name='search.tasks.get_trains', default_retry_delay=1 * 60, max_retries=60)
def get_trains(data, attempt, res_base):
    if attempt == 1:
        base_dict = json.loads(res_base)
        print  base_dict
    if attempt > 50:
        base_dict = json.loads(res_base)
        message = u"Час виконання пошуку квитків між станціями {0}-{1} закінчився. Для повторного пошуку перейдіть на http://train-tickets.com.ua/".format(base_dict.get('nstotpr'), base_dict.get('nstprib'))
        short_message = u"Час пошуку {0}-{1} закінчився".format(base_dict.get('nstotpr')[:5], base_dict.get('nstprib')[:5])
        send_message(data, message, short_message)
        return 'success'
    res = urllib2.urlopen('http://www.pz.gov.ua/rezerv/aj_g60.php', data['query']).read()
    if not res_base:
        res_base = res
    attempt += 1
    countdown = settings.BASE_REQUEST_DELAY*(math.log(attempt+5)-1)
    print "countdown:"+str(countdown)

    if res == res_base:
        print 'no change in tickets'
        print str(attempt)
        return get_trains.retry(args=[data, attempt, res_base], countdown=countdown)

    base_dict = json.loads(res_base)


    if base_dict.get('trains'):
        base_trains = base_dict['trains']
    else:
        print "no trains in base_resp retrying with resp"
        return get_trains.retry(args=[data, attempt, res], countdown=countdown)

    msg = u"Нові квитки: "
    short_msg = u""
    res_dict = json.loads(res)

    if res_dict.get('trains'):
        print "I'm in trains"
        for train in res_dict['trains']:
            base_train = [base_train for i,base_train in enumerate(base_trains) if base_train['train'] == train['train']]
            if base_train:
                base_train = base_train[0]
                if data['kype']:
                    if base_train['k'] < train['k']:
                        print 'new k'
                        msg += u'Купейних місць {0} замісць {1} на потяг номер {2}, {3}-{4}. Відправлення {5}, прибуття {6}. '.format(str(train['k']), str(base_train['k']), train['train']['0'], train['from']['0'], train['to']['0'], train['otpr'], train['prib'])
                        if u"потяг" in short_msg:
                            short_msg += u"; "
                        else:
                            short_msg += u"потяг "
                        short_msg += u'{1},купе:{0},відпр:{2}'.format(str(train['k']), train['train']['0'], train['otpr'])
                if data['platzcart']:
                    if base_train['p'] < train['p']:
                        print 'new p'
                        msg += u'Плацкартних місць {0} замісць {1} потяг номер {2}, {3}-{4}. Відправлення {5}, прибуття {6}. '.format(str(train['p']), str(base_train['p']), train['train']['0'], train['from']['0'], train['to']['0'], train['otpr'], train['prib'])
                        if u"потяг" in short_msg:
                            short_msg += u"; "
                        else:
                            short_msg += u"потяг "
                        short_msg += u'{1},плацк:{0},відпр:{2}'.format(str(train['p']), train['train']['0'], train['otpr'])
            elif train['k'] != 0 or train['p'] != 0:
                msg+=u"новий потяг номер {0} сполученням {1}-{2} з {3} купейними місцями і {4} плацкартними. Відправлення {5}, прибуття {6}.".format(train['train']['0'], train['from']['0'], train['to']['0'], train['k'], train['p'], train['otpr'], train['prib'])
                if u"потяг" in short_msg:
                    short_msg += u"; "
                else:
                    short_msg += u"потяг "
                short_msg+=u"{0},купе:{3},плацк:{4},відпр:{5}".format(train['train']['0'], train['from']['0'], train['to']['0'], train['k'], train['p'], train['otpr'])
            else:
                print 'we are not in new train nor in usual'
                print train['k'], train['p']
        if msg != u"Нові квитки: ":
            msg += u"Для купівлі квитків перейдіть на http://booking.uz.gov.ua/ "
            send_message(data, msg, short_msg)
        return get_trains.retry(args=[data, attempt, res], countdown=countdown)
    else:
        print "no trains in response {0}".format(str(res_dict))
        return get_trains.retry(args=[data, attempt, res_base], countdown=countdown)